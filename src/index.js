import es6Promise from 'es6-promise';
import RESTClient from 'rest-client';
es6Promise.polyfill()

import esc from 'jquery-param';

const LOCALSTORAGE_KEY = 'redpine-api-client';

class RedpineAPIClient extends RESTClient {
  constructor(baseUrl) {
    super(baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    })
    
    this.venueListingsRegister = this._resource({
      route: 'venue-listings/registration/password'
    })

    this.facebookLogIn = this._oauth({
      route: '/auth/facebook/'
    })

    this.register = this._resource({
      route: 'registration/password'
    })

    this.facebookRegister = this._resource({
      route: 'registration/facebook'
    })

    this.forgotPassword = this._resource({
      route: 'auth/forgot-password'
    })

    this.resetPassword = this._resource({
      route: 'auth/reset-password'
    })

    this.spotifyConnection = this._resource({
      route: 'spotify/connection'
    })

    this.nylasConnection = this._resource({
      route: 'nylas/connection'
    })

    this.socanVerification = this._resource({
      route: 'socan/verification'
    })

    this.socanVerificationStatus = this._resource({
      route: 'socan/verification-status'
    })

    this.campaignFact = this._resource({
      route: 'analysis/campaign-fact'
    })

    this.pledgeFact = this._resource({
      route: 'analysis/pledge-fact',
      details: [
        { routeName: 'related_bands', method: 'get' }
      ]
    })

    this.timeslotFact = this._resource({
      route: 'analysis/timeslot-fact'
    })

    this.coverLeads = this._resource({
      route: 'cover-bands/cover-leads'
    })

    this.users = this._resource({
      route: 'users'
    })

    this.notifications = this._resource({
      route: 'notifications',
      details: [
        { routeName: 'unread_count', method: 'get' },
        { routeName: 'read', method: 'post' },
        { routeName: 'read_all', method: 'post' },
        { routeName: 'delete_all', method: 'post' }
      ]
    })

    this.bands = this._resource({
      route: 'bands',
      details: [
        { routeName: 'payout', method: 'post' }
      ]
    })

    this.openings = this._resource({
      route: 'openings'
    })

    this.events = this._resource({
      route: 'events'
    })

    this.tracks = this._resource({
      route: 'tracks'
    })

    this.trackComments = this._resource({
      route: 'track-comments'
    })

    this.campaignDocuments = this._resource({
      route: 'campaign-documents'
    })

    this.campaignBands = this._resource({
      route: 'campaign-bands'
    })

    this.purchaseItems = this._resource({
      route: 'purchase-items'
    })

    this.profiles = this._resource({
      route: 'profiles',
      details: [
        { routeName: 'confirm', method: 'get' }
      ]
    })

    this.campaigns = this._resource({
      route: 'campaigns',
      details: [
        { routeName: 'confirm', method: 'post' },
        { routeName: 'deny', method: 'post' },
        { routeName: 'book', method: 'post' },
        { routeName: 'door_code', method: 'get' },
      ],
      lists: [
        { routeName: 'check_door_code', method: 'post' },
      ]
    })

    this.campaignFeed = this._resource({
      route: 'campaign-feed'
    })

    this.tourCampaigns = this._resource({
      route: 'tour-campaigns'
    })

    this.tours = this._resource({
      route: 'tours'
    })

    this.pledges = this._resource({
      route: 'pledges'
    })

    this.tickets = this._resource({
      route: 'tickets',
      lists: [
        { routeName: 'validate', method: 'post' }
      ]
    })

    this.guestList = this._resource({
      route: 'guest-list'
    })

    this.venues = this._resource({
      route: 'venues'
    })

    this.organizationBands = this._resource({
      route: 'organization-bands',
      details: [
        { routeName: 'confirm', method: 'post' },
        { routeName: 'deny', method: 'post' }
      ]
    })

    this.organizations = this._resource({
      route: 'organizations',
      details: [
        { routeName: 'payout', method: 'post' }
      ]
    })

    this.cities = this._resource({
      route: 'cities',
      details: [
        { routeName: 'with_bands', method: 'get' },
        { routeName: 'with_venues', method: 'get' }
      ]
    })

    this.provinces = this._resource({
      route: 'provinces',
      details: [
        { routeName: 'with_bands', method: 'get' },
        { routeName: 'with_venues', method: 'get' }
      ]
    })

    this.countries = this._resource({
      route: 'countries',
      details: [
        { routeName: 'with_bands', method: 'get' },
        { routeName: 'with_venues', method: 'get' }
      ]
    })

    this.timeslots = this._resource({
      route: 'timeslots'
    })

    this.hints = this._resource({
      route: 'hints'
    })

    this.genres = this._resource({
      route: 'genres',
      details: [
        { routeName: 'root', method: 'get' }
      ]
    })
    
    this.bookingRequests = this._resource({
      route: 'booking-requests'
    })

    this.accountSubscriptions = this._resource({
      route: 'account-subscriptions'
    })

    this.bandSubscriptions = this._resource({
      route: 'band-subscriptions'
    })

    this.organizationSubscriptions = this._resource({
      route: 'organization-subscriptions'
    })

    this.venueSubscriptions = this._resource({
      route: 'venue-subscriptions'
    })

    this.bandToBandReviews = this._resource({
      route: 'band-to-band-reviews'
    })

    this.bandToVenueReviews = this._resource({
      route: 'band-to-venue-reviews'
    })

    this.venueToBandReviews = this._resource({
      route: 'venue-to-band-reviews'
    })

    this.invitations = this._resource({
      route: 'invitations'
    })

    this.showRequests = this._resource({
      route: 'show-requests'
    })

    this.justTickets = this._resource({
      route: 'just-tickets'
    })

    this.messages = this._resource({
      route: 'messages',
      details: [
        { routeName: 'unread', method: 'get' },
        { routeName: 'conversations', method: 'get' },
        { routeName: 'conversation', method: 'get' },
        { routeName: 'send', method: 'post' },
        { routeName: 'read', method: 'post' },
        { routeName: 'read_formatted', method: 'post' }
      ]
    })

    this.actPayments = this._resource({
      route: 'act-payments'
    })

    this.paymentRequests = this._resource({
      route: 'payment-requests'
    })

    this.rewards = this._resource({
      route: 'rewards'
    })

    this.globalSettings = this._resource({
      route: 'global-settings'
    })
    
    this.me = this._resource({
      route: 'me'
    })
    
    this.navigationFeedback = this._resource({
      route: 'navigation-feedback'
    })

    this.appCashTransactions = this._resource({
      route: 'app-cash-transactions'
    })

    this.appCardTransactions = this._resource({
      route: 'app-card-transactions',
      details: [
        { routeName: 'square_pos_callback', method: 'post' }
      ]
    })

    this.pushTokens = this._resource({
      route: 'push-tokens',
      lists: [
        { routeName: 'delete_by_token', method: 'delete' },
      ]
    })
  }
}

export default RedpineAPIClient;

export { LOCALSTORAGE_KEY }