import assert from 'assert';
import RedpineAPIClient from '../src';
import nock from 'nock';
import fetchEverywhere from 'fetch-everywhere';
import {LocalStorage} from 'node-localstorage';
import {LOCALSTORAGE_KEY} from '../src';

var BASE = 'http://localhost:8000';
global.localStorage = new LocalStorage('./.tmp');

describe('basic', () => {
  it('should export properly', () => {
    var client = new RedpineAPIClient('http://127.0.0.1')
  })
})

describe('oauth', () => {
  it('should call the correct route & log user in', () => {
    var client = new RedpineAPIClient(BASE)

    nock(BASE)
      .post('/auth/facebook/')
      .reply(200, {
        token: 'wat'
      })

    return client.facebookLogIn({
      code: 'test',
      redirect_uri: 'test'
    }).then((res) => {
      assert.equal(client.token, 'wat')
    })
  })
})

describe('auth', () => {
  it('should call correct logout url', () => {
    var client = new RedpineAPIClient(BASE)

    nock(BASE)
      .post('/auth/logout/')
      .reply(200, {})

    client.token = 'token'
    client.user = 1
    return client.logOut()
  })
})