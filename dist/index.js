'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOCALSTORAGE_KEY = undefined;

var _es6Promise = require('es6-promise');

var _es6Promise2 = _interopRequireDefault(_es6Promise);

var _restClient = require('rest-client');

var _restClient2 = _interopRequireDefault(_restClient);

var _jqueryParam = require('jquery-param');

var _jqueryParam2 = _interopRequireDefault(_jqueryParam);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

_es6Promise2.default.polyfill();

var LOCALSTORAGE_KEY = 'redpine-api-client';

var RedpineAPIClient = function (_RESTClient) {
  _inherits(RedpineAPIClient, _RESTClient);

  function RedpineAPIClient(baseUrl) {
    _classCallCheck(this, RedpineAPIClient);

    var _this = _possibleConstructorReturn(this, (RedpineAPIClient.__proto__ || Object.getPrototypeOf(RedpineAPIClient)).call(this, baseUrl, {
      AUTH_PATH: '/auth/password/',
      LOGOUT_PATH: '/auth/logout/'
    }));

    _this.venueListingsRegister = _this._resource({
      route: 'venue-listings/registration/password'
    });

    _this.facebookLogIn = _this._oauth({
      route: '/auth/facebook/'
    });

    _this.register = _this._resource({
      route: 'registration/password'
    });

    _this.facebookRegister = _this._resource({
      route: 'registration/facebook'
    });

    _this.forgotPassword = _this._resource({
      route: 'auth/forgot-password'
    });

    _this.resetPassword = _this._resource({
      route: 'auth/reset-password'
    });

    _this.spotifyConnection = _this._resource({
      route: 'spotify/connection'
    });

    _this.nylasConnection = _this._resource({
      route: 'nylas/connection'
    });

    _this.socanVerification = _this._resource({
      route: 'socan/verification'
    });

    _this.socanVerificationStatus = _this._resource({
      route: 'socan/verification-status'
    });

    _this.campaignFact = _this._resource({
      route: 'analysis/campaign-fact'
    });

    _this.pledgeFact = _this._resource({
      route: 'analysis/pledge-fact',
      details: [{ routeName: 'related_bands', method: 'get' }]
    });

    _this.timeslotFact = _this._resource({
      route: 'analysis/timeslot-fact'
    });

    _this.coverLeads = _this._resource({
      route: 'cover-bands/cover-leads'
    });

    _this.users = _this._resource({
      route: 'users'
    });

    _this.notifications = _this._resource({
      route: 'notifications',
      details: [{ routeName: 'unread_count', method: 'get' }, { routeName: 'read', method: 'post' }, { routeName: 'read_all', method: 'post' }, { routeName: 'delete_all', method: 'post' }]
    });

    _this.bands = _this._resource({
      route: 'bands',
      details: [{ routeName: 'payout', method: 'post'}]
    });

    _this.openings = _this._resource({
      route: 'openings'
    });

    _this.events = _this._resource({
      route: 'events'
    });

    _this.tracks = _this._resource({
      route: 'tracks'
    });

    _this.trackComments = _this._resource({
      route: 'track-comments'
    });

    _this.campaignDocuments = _this._resource({
      route: 'campaign-documents'
    });

    _this.campaignBands = _this._resource({
      route: 'campaign-bands'
    });

    _this.purchaseItems = _this._resource({
      route: 'purchase-items'
    });

    _this.profiles = _this._resource({
      route: 'profiles',
      details: [{ routeName: 'confirm', method: 'get' }]
    });

    _this.campaigns = _this._resource({
      route: 'campaigns',
      details: [{ routeName: 'confirm', method: 'post' }, { routeName: 'deny', method: 'post' }, { routeName: 'book', method: 'post' }, { routeName: 'door_code', method: 'get' }],
      lists: [{ routeName: 'check_door_code', method: 'post' }]
    });

    _this.campaignFeed = _this._resource({
      route: 'campaign-feed'
    });

    _this.tourCampaigns = _this._resource({
      route: 'tour-campaigns'
    });

    _this.tours = _this._resource({
      route: 'tours'
    });

    _this.pledges = _this._resource({
      route: 'pledges'
    });

    _this.tickets = _this._resource({
      route: 'tickets',
      lists: [{ routeName: 'validate', method: 'post' }]
    });

    _this.guestList = _this._resource({
      route: 'guest-list'
    });

    _this.venues = _this._resource({
      route: 'venues'
    });

    _this.organizationBands = _this._resource({
      route: 'organization-bands',
      details: [{ routeName: 'confirm', method: 'post' }, { routeName: 'deny', method: 'post' }]
    });

    _this.organizations = _this._resource({
      route: 'organizations',
      details: [{ routeName: 'payout', method: 'post'}]
    });

    _this.cities = _this._resource({
      route: 'cities',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.provinces = _this._resource({
      route: 'provinces',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.countries = _this._resource({
      route: 'countries',
      details: [{ routeName: 'with_bands', method: 'get' }, { routeName: 'with_venues', method: 'get' }]
    });

    _this.timeslots = _this._resource({
      route: 'timeslots'
    });

    _this.hints = _this._resource({
      route: 'hints'
    });

    _this.genres = _this._resource({
      route: 'genres',
      details: [{ routeName: 'root', method: 'get' }]
    });

    _this.bookingRequests = _this._resource({
      route: 'booking-requests'
    });

    _this.accountSubscriptions = _this._resource({
      route: 'account-subscriptions'
    });

    _this.bandSubscriptions = _this._resource({
      route: 'band-subscriptions'
    });

    _this.organizationSubscriptions = _this._resource({
      route: 'organization-subscriptions'
    });

    _this.venueSubscriptions = _this._resource({
      route: 'venue-subscriptions'
    });

    _this.bandToBandReviews = _this._resource({
      route: 'band-to-band-reviews'
    });

    _this.bandToVenueReviews = _this._resource({
      route: 'band-to-venue-reviews'
    });

    _this.venueToBandReviews = _this._resource({
      route: 'venue-to-band-reviews'
    });

    _this.invitations = _this._resource({
      route: 'invitations'
    });

    _this.showRequests = _this._resource({
      route: 'show-requests'
    });

    _this.justTickets = _this._resource({
      route: 'just-tickets'
    });

    _this.messages = _this._resource({
      route: 'messages',
      details: [{ routeName: 'unread', method: 'get' }, { routeName: 'conversations', method: 'get' }, { routeName: 'conversation', method: 'get' }, { routeName: 'send', method: 'post' }, { routeName: 'read', method: 'post' }, { routeName: 'read_formatted', method: 'post' }]
    });

    _this.actPayments = _this._resource({
      route: 'act-payments'
    });

    _this.paymentRequests = _this._resource({
      route: 'payment-requests'
    });

    _this.rewards = _this._resource({
      route: 'rewards'
    });

    _this.globalSettings = _this._resource({
      route: 'global-settings'
    });

    _this.me = _this._resource({
      route: 'me'
    });

    _this.navigationFeedback = _this._resource({
      route: 'navigation-feedback'
    });

    _this.appCashTransactions = _this._resource({
      route: 'app-cash-transactions'
    });

    _this.appCardTransactions = _this._resource({
      route: 'app-card-transactions',
      details: [{ routeName: 'square_pos_callback', method: 'post' }]
    });

    _this.pushTokens = _this._resource({
      route: 'push-tokens',
      lists: [{ routeName: 'delete_by_token', method: 'delete' }]
    });
    return _this;
  }

  return RedpineAPIClient;
}(_restClient2.default);

exports.default = RedpineAPIClient;
exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
