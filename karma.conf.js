module.exports = function(config) {
  config.set({
    browsers: ['Chrome'],
    frameworks: ['mocha'],

    files: [
      'src/index.js',
      'spec/**/*.js'
    ],
    preprocessors: {
      'src/index.js': ['babel'],
      'spec/**/*.js': ['babel'],
    },
    "babelPreprocessor": {
      options: {
        presets: ['es2015'],
        plugins: ['transform-es2015-modules-umd']
      }
    }
  });
};